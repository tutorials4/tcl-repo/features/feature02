#include <sub.hpp>
#include <div.hpp>

namespace tcl
{
namespace feature02
{
class Engine
{
public:
    Engine();
    ~Engine();

    //! perform (c-d)/(a+b)
    void process(int a, int b, int c, int d);

    int result() const;

private:
    int _result;
    subtraction::Engine _subtraction;
    division::Engine _division;
};
} // namespace feature02
} // namespace tcl
