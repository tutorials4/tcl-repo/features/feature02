#include <iostream>
#include <feature02.hpp>

int main() {
    std::cout << "Test Start..." << std::endl;
    tcl::feature02::Engine feature02;

    feature02.process(3,1,10,6);
    std::cout << feature02.result() << std::endl;
    if (feature02.result() != 1) {
        return 1;
    }
    return 0;
}
